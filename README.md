# Gnome Viewer#

Gnome Viewer is an Android application that loads a set of gnomes from a remote file in JSON format and present them in a list. 
It allows to filter gnomes by name, show details and navigate through gnome's friends.

### Usage: ###

* Scroll down the list to load more gnomes.
* Click the search button to show or hide the filter EditText.
* Type in the filter EditText to filter gnomes by name.
* Click on more details link to load Gnome details in a Dialog.
* Navigate through friends clicking on their names.

### Implementation ###

This app implements MVP pattern and uses the following libraries: 

* Picasso to load images from Internet and cache them. 
* Retrofit to make REST calls.
* Gson as a Retrofit converter to read a JSON and deserialize into Java objects.

### Classes summary ###

View

* MainView - Interface with methods for manage the view.
* MainActivity - Main activity of the app. Implements MainView interface. It notifies the Presenter when the user interacts with the view.
* CustomListAdapter - Custom adapter for MainActivity List.

Presenter

* Presenter - Receives data and passes it formatted to the app view. It also decides what happens when the user interact with the view.
* DataAccess - Interface with methods for accessing data.
* GnomeDataAccess - Access gnome data and passes it to the presenter. It implements DataAccess interface.
* AxaGnomeService - Interface which defines REST services from remote provider. It uses Retrofit library.
* DownloadGnomesTask - Task to download gnome data asynchronously using Retrofit.

Model

* Gnome - Class which represents a Gnome.
* GnomeCity - Class which represents a Gnome city.

### Source code. ###

https://bitbucket.org/kiwydev/axa-assesment/src