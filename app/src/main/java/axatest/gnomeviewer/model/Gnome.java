package axatest.gnomeviewer.model;

import java.util.Random;

/**
 * Class that represents a gnome
 */
public class Gnome {

	/**Gnome identifier*/
	public int id;

	/**Gnome name*/
	public String name;

	/**Gnome thumbnail URL*/
	public String thumbnail;

	/**Gnome age in years*/
	public int age;

	/**Gnome weight*/
	public float weight;

	/**Gnome height*/
	public float height;

	/**Gnome hair_color*/
	public String hair_color;

	/**Gnome professions*/
	public String[] professions;

	/**Gnome friends*/
	public String[] friends;


	/**
	 * Return a list with gnome professions
	 *
	 * @return String with professions or none
	 *
	 */
	public String getProfessionsString(){
		String prof = "None";
		if(getProfessions().length > 0) {
			prof = getProfessions()[0];
			for (int i = 1; i < getProfessions().length; i++) {
				prof += ", "+getProfessions()[i];
			}
		}
		return prof;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	public String getHair_color() {
		return hair_color;
	}

	public void setHair_color(String hair_color) {
		this.hair_color = hair_color;
	}

	public String[] getProfessions() {
		return professions;
	}

	public void setProfessions(String[] professions) {
		this.professions = professions;
	}

	public String[] getFriends() {
		return friends;
	}

	public void setFriends(String[] friends) {
		this.friends = friends;
	}
	

}
