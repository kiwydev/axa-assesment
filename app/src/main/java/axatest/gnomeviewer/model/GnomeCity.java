package axatest.gnomeviewer.model;

import java.util.List;

/**
 * Class which represents a gnome city with a list of inhabitants.
 */
public class GnomeCity {

	/**List of gnomes from Brastlewark*/
	List<Gnome> Brastlewark;

	/**
	 * Search for a gnome with the specified name
	 *
	 * @param gnomeName The gnome name
	 * @return The first coincidence of a gnome with this name or null if not found.
	 */
	public Gnome findByName(String gnomeName){
		boolean found = false;
		Gnome coincidence = null;
		for (int i = 0; i < Brastlewark.size() && !found; i++) {
			if(gnomeName.equals(Brastlewark.get(i).getName())){
				coincidence = Brastlewark.get(i);
			}
		}
		return coincidence;
	}

	public List<Gnome> getBrastlewark() {
		return Brastlewark;
	}

	public void setBrastlewark(List<Gnome> brastlewark) {
		Brastlewark = brastlewark;
	}

}
