package axatest.gnomeviewer.presenter;


import axatest.gnomeviewer.model.GnomeCity;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by pmartinb on 10/04/2016.
 * Defines REST services from https://raw.githubusercontent.com/AXA-GROUP-SOLUTIONS
 */
public interface AxaGnomeService {

    /**
     * Obtain gnome population from a remote file
     *
     * @param filename the name of the file
     * @return A GnomeCity object with the gnome population
     */
    @GET("mobilefactory-test/master/{filename}")
    Call<GnomeCity> listGnomesFromRemoteFile(@Path("filename") String filename);
}
