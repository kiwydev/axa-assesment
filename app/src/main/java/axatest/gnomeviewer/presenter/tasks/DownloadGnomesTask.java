package axatest.gnomeviewer.presenter.tasks;

import android.os.AsyncTask;

import axatest.gnomeviewer.presenter.AxaGnomeService;
import axatest.gnomeviewer.presenter.GnomeDataAccess;
import axatest.gnomeviewer.model.GnomeCity;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by pmartinb on 17/10/2015.
 * Class to download gnome population data asynchronously.
 */
public class DownloadGnomesTask extends AsyncTask<String, Void, String[]> {

    /**Data access class to pass the downloaded data.*/
    GnomeDataAccess gnomeDataAccess;

    /**
     * Creates DownloadGnomesTask object.
     *
     * @param gnomeDataAccess data access class.
     */
    public DownloadGnomesTask(GnomeDataAccess gnomeDataAccess){
        super();
        this.gnomeDataAccess = gnomeDataAccess;
    }

    /**
     * Download gnome population data from a URL.
     *
     * @param urls URL where the data is stored.
     * @return Response object with gnome data or error message.
     */
    protected String[] doInBackground(String... urls) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(urls[0])
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        String[] responseStatus  = new String[2];
        AxaGnomeService service = retrofit.create(AxaGnomeService.class);
        GnomeCity gnomeCity = null;
        Response<GnomeCity> response = null;
        try {
            Call<GnomeCity> gnomes = service.listGnomesFromRemoteFile("data.json");
            response= gnomes.execute();
            gnomeCity = response.body();
            gnomeDataAccess.setGnomeCity(gnomeCity);
            responseStatus[0] = response.code()+"";
            responseStatus[1] = response.code()+" "+response.message();
        }catch (Exception e){
            e.printStackTrace();
            responseStatus[0] = "-1";
            responseStatus[1] = e.getClass().getCanonicalName()+" "+e.getLocalizedMessage();
        }
        return responseStatus;
    }


    /**
     * Called when background task is finished. Notify data access class that gnomes were downloaded successfully
     * or that an error occurred.
     *
     * @param response Response object with gnome data or error message.
     */
    protected void onPostExecute(String[] response) {
        if (response[0].equals("200")) {
            gnomeDataAccess.onSuccess();
        }else{
            gnomeDataAccess.onError(response[1]);
        }

    }

    @Override
    protected void onPreExecute() {
    }

}
