package axatest.gnomeviewer.presenter;

/**
 * Created by pmartinb on 09/04/2016.
 * Interface for accessing data
 *
 */
public interface DataAccess {

    /**
     * Load data from a data source
     */
    void loadData();

    /**
     * Called when data is loaded succesfully
     */
    void onSuccess();

    /**
     * Called when there were an error loading data
     *
     * @param errorMessage The error message
     */
    void onError(String errorMessage);

}
