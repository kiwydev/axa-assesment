package axatest.gnomeviewer.presenter;

import android.util.Log;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import axatest.gnomeviewer.model.GnomeCity;
import axatest.gnomeviewer.presenter.tasks.DownloadGnomesTask;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by pmartinb on 09/04/2016.
 *
 * Access data from Gnome population data source
 */
public class GnomeDataAccess implements DataAccess{

    /**City whith the gnome population*/
    GnomeCity gnomeCity = null;

    /**Presenter of the MVP pattern*/
    Presenter presenter;

    /**Base URL for gnome population data source*/
    String baseUrl;

    /**
     * Creates a GnomeDataAccess object for a datasource.
     *
     * @param presenter The presenter of MVP pattern
     * @param baseUrl The base URL for data source
     */
    public GnomeDataAccess(Presenter presenter, String baseUrl){
        this.presenter = presenter;
        this.baseUrl = baseUrl;
    }

    /**
     * Load Gnome population from defined data source
     */
    public void loadData(){
        DownloadGnomesTask downloadTask = new DownloadGnomesTask(this) ;
        downloadTask.execute(baseUrl);
    }

    /**
     * Notify the Presenter that gnome data has ben loaded correctly.
     */
    public void onSuccess(){
        presenter.onGnomeLoadedSuccess(gnomeCity.getBrastlewark());
    }

    /**
     * Notify the presenter that an error occurred during Gnome data load
     *
     * @param errorMessage The error message
     */
    public void onError(String errorMessage){
        presenter.onGnomeLoadedError("Can't load gnomes: " + errorMessage);
    }

    /**
     * @return current gnome city
     */
    public GnomeCity getGnomeCity() {
        return gnomeCity;
    }

    /**
     * @param gnomeCity to set
     */
    public void setGnomeCity(GnomeCity gnomeCity) {
        this.gnomeCity = gnomeCity;
    }
}
