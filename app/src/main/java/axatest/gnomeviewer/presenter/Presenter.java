package axatest.gnomeviewer.presenter;

import android.app.Activity;
import android.view.View;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.List;

import axatest.gnomeviewer.R;
import axatest.gnomeviewer.view.CustomListAdapter;
import axatest.gnomeviewer.view.MainActivity;
import axatest.gnomeviewer.view.MainView;
import axatest.gnomeviewer.model.Gnome;

/**
 * Created by pmartinb on 09/04/2016.
 *  Class which retrieves data from gnome population and passes it formatted to the app view.
 *  It also decides what happens when the user interact with the view.
 *
 *  This class implements the presenter of the MVP pattern.
 *
 */
public class Presenter {

    /** The view of the MVP pattern.*/
    MainView view;

    /** Data accessor to gnome population.*/
    DataAccess gnomeDataAccess;

    /**
     * Creates a Presenter object.
     *
     * @param view The view of the MVP pattern.
     */
    public Presenter(MainActivity view){
        this.view = view;
        gnomeDataAccess = new GnomeDataAccess(this, view.getString(R.string.axa_services_base_url));
        gnomeDataAccess.loadData();
        this.view.showProgress();
    }

    /**
     * Updates the view with loaded gnomes.
     *
     * @param items The list of gnomes loaded.
     */
    public void onGnomeLoadedSuccess(List<Gnome> items){
        view.loadElements(items);
        view.setFilter();
        this.view.hideProgress();
    }

    /**
     * Notifies the view that there were an error loading gnomes.
     *
     * @param errorMessage The error message.
     */
    public void onGnomeLoadedError(String errorMessage){
        view.showErrorDialog(errorMessage);
        this.view.hideProgress();
    }

    /**
     * Finishes app.
     */
    public void onExit(){
        ((Activity)view).finish();
    }

    /**
     * Shows edit text to filter gnome list or hide it if it was already visible.
     *
     * @param editTextFilter The edit text to show or hyde.
     */
    public void searchButtonClicked(final EditText editTextFilter){
        if (editTextFilter.getVisibility() == View.INVISIBLE)
            editTextFilter.setVisibility(View.VISIBLE);
        else {
            editTextFilter.setText("");
            view.cleanSearchEditText();
            editTextFilter.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * Filters elements of the view (gnomes) whose name starts with a prefix.
     *
     * @param cs The prefix (case insensitive)
     */
    public void filterTextChanged(CharSequence cs){
        (((MainActivity)view).getAdapter()).getFilter().filter(cs);
    }

    /**
     * Informs the view to display a dialog with all the details of a gnome.
     *
     * @param gnome The gnome to display.
     * @param photo The picture of the gnome.
     */
    public void moreDetailsClicked(final Gnome gnome, ImageView photo){
        view.showItemDetailsDialog(gnome, photo);
    }

    /**
     * Finds a gnome with the given name and informs the view to display it's details in a dialog.
     *
     * @param friendName The friend name.
     */
    public void friendClicked(String friendName){
        Gnome friend = ((GnomeDataAccess) gnomeDataAccess).getGnomeCity().findByName(friendName);
        view.showItemDetailsDialog(friend, null);
    }

    /**
     * Called when user do scroll. Inform the list adapter to load more elements to the list.
     *
     * @param view The view whose scroll state is being reported.
     * @param firstVisible the index of the first visible cell (ignore if visibleItemCount == 0).
     * @param visibleCount the number of visible cells.
     * @param totalCount the number of items in the list adaptor.
     */
    public void onScroll(AbsListView view, int firstVisible, int visibleCount, int totalCount) {
        boolean loadMore = firstVisible + visibleCount >= totalCount;
        if(loadMore) {
            ((CustomListAdapter)view.getAdapter()).count += visibleCount;
            ((CustomListAdapter)view.getAdapter()).notifyDataSetChanged();
        }
    }
}
