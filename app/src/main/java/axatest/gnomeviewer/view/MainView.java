package axatest.gnomeviewer.view;

import android.graphics.Typeface;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import axatest.gnomeviewer.model.Gnome;

/**
 * Created by pmartinb on 09/04/2016.
 * Interface for the app main view.
 */
public interface MainView {

    /**
     * Loads elements for the view.
     *
     * @param items A list of the view elements.
     */
    public void loadElements(List<?> items);

    /**
     * Sets a filter for the elements of the view.
     */
    public void setFilter();

    /**
     * Shows a ProgressDialog
     */
    public void showProgress();

    /**
     * Hides a ProgressDialog if it is showing.
     */
    public void hideProgress();

    /**
     * Clears filter EditText.
     */
    public void cleanSearchEditText();

    /**
     * Show a Dialog with an item details
     *
     * @param item item to show
     * @param photo item thumbnail if already loaded (null if not)
     *
     */
    public void showItemDetailsDialog(final Object item, ImageView photo);

    /**
     * Shows a dialog with an error message.
     *
     * @param errorMessage The error message.
     */
    public void showErrorDialog(String errorMessage);

    /**
     * Format TextView with specified parameters
     *
     * @param tv TextView to format
     * @param size size of the TextView
     * @param typeFace font of the TextView
     * @param idColor color id of the TextView
     *
     */
    public void formatTextView(TextView tv,  int size, Typeface typeFace, int idColor);
}
