package axatest.gnomeviewer.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import axatest.gnomeviewer.R;
import axatest.gnomeviewer.model.Gnome;

/**
 * Custom adapter for MainActivity ListView. This adapter is backed by a list of Gnome objects.
 */
public class CustomListAdapter extends ArrayAdapter<Gnome> implements Filterable {

	/**Context of the adapter.*/
	private Context context;

	/**Gnomes actually loaded by the list. They can be filtered.*/
	List<Gnome> items;

	/**All gnomes (without filters).*/
	List<Gnome> allGnomes;

	/**Starting amount of elements shown in the list*/
	public int count = 7;

	/**True if a toast message is being shown*/
	boolean toastShown = false;

	/**
	 * Creates a CustomListAdapter for a list view.
	 *
	 * @param context  The current context.
	 * @param layoutResourceId The resource ID for a layout file containing a layout to use when instantiating views
	 * @param items The gnomes to represent in the ListView.
	 */
	public CustomListAdapter(Context context, int layoutResourceId, List<Gnome> items) {
	        super(context, layoutResourceId, items);
	        this.context = context;
	        this.items = items;
			this.allGnomes = items;
	    }


	/**
	 * {@inheritDoc}
	 */
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View row = inflater.inflate(R.layout.gnome_row, parent, false);
		if(position >= items.size()) return row;
		Typeface typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/AaronSans.ttf"); //$NON-NLS-1$
		TextView nameTV = (TextView) row.findViewById(R.id.nameTextView);
		nameTV.setText(items.get(position).getName());
		((MainView) context).formatTextView(nameTV, 19, typeFace, context.getResources().getColor(R.color.android_green));
		((TextView) row.findViewById(R.id.ageTextView)).setText(context.getString(R.string.Age_value, items.get(position).getAge()));
		((TextView) row.findViewById(R.id.weightTextView)).setText(context.getString(R.string.Weight_value, items.get(position).getWeight()));
		((TextView) row.findViewById(R.id.heightTextView)).setText(context.getString(R.string.Height_value, items.get(position).getHeight()));
		((TextView) row.findViewById(R.id.hairColorTextView)).setText(context.getString(R.string.Hair_color_value, items.get(position).getHair_color()));
		TextView moreDetailsLink = ((TextView) row.findViewById(R.id.detailsTextView));
		moreDetailsLink.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				((MainActivity) context).getPresenter().moreDetailsClicked(items.get(position), (ImageView) row.findViewById(R.id.thumbImageView));
			}
		});
		ImageView thumb = (ImageView) row.findViewById(R.id.thumbImageView);
		String imageUri = items.get(position).getThumbnail();
		Picasso.with(context).load(imageUri).into(thumb);
		return row;
	}



	/**
	 * @return number of gnomes actually loaded by the list. They can be filtered.
	 */
	public int getCount() { return count; }

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isEnabled(int position) {
		return false;
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public Filter getFilter() {
		Filter filter = new Filter() {

			/**
			 * Updates list elements with the filtered results.
			 *
			 * @param constraint prefix to compare. Used in the filter.
			 * @param results the filtered gnomes.
			 */
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				items = (List<Gnome>) results.values;
				notifyDataSetChanged();
			}

			/**
			 * Filter gnomes by name. Case insensitive.
			 *
			 * @param constraint prefix to compare.
			 * @return the filtered gnomes.
			 */
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				items = allGnomes;
				FilterResults results = new FilterResults();
				ArrayList<Gnome> FilteredArrayNames = new ArrayList<Gnome>();
				constraint = constraint.toString().toLowerCase();
				for (int i = 0; i < items.size(); i++) {
					Gnome filteredGnome = items.get(i);
					if (filteredGnome.getName().toLowerCase().startsWith(constraint.toString().toLowerCase()))  {
						FilteredArrayNames.add(filteredGnome);
					}
				}
				results.count = FilteredArrayNames.size();
				results.values = FilteredArrayNames;
				if(results.count ==0 && !toastShown){
					Log.i("CustomListAdapter", "No ocurrences found ");
					Toast.makeText(context, "No ocurrences found", Toast.LENGTH_SHORT).show();
					toastShown = true;
				}else if(results.count >0){
					toastShown = false;
				}
				return results;
			}
		};
		return filter;
	}


}
