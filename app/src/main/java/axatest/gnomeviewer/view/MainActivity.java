package axatest.gnomeviewer.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import axatest.gnomeviewer.R;
import axatest.gnomeviewer.model.Gnome;
import axatest.gnomeviewer.presenter.Presenter;

/**
 * Created by pmartinb on 09/04/2016.
 * Represents the main screen of the app. This Activity will present data inside a list.
 *
 */
public class MainActivity extends ListActivity implements MainView,  AbsListView.OnScrollListener {

    /**Custom adapter for the ListActivity*/
    CustomListAdapter adapter;

    /**Progress dialog to show while loading data*/
    ProgressDialog progressDialog;

    /**Presenter of the MVP pattern*/
    private Presenter presenter;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gnome_list);
        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("Loading gnomes...");
        presenter = new Presenter(this);
    }

    /**
     * Load elements to be shown in this view.
     *
     * @param items A list of the view elements.
     */
    public void loadElements(List<?> items){
        adapter= new CustomListAdapter(this,R.layout.gnome_row,(List<Gnome>)(List<?>)items);
        setListAdapter(adapter);
        getListView().setOnScrollListener(this);
    }

    /**
     * Sets a filter for the elements of the view. It configures an EditText to filter gnomes
     * and notify Presenter when its text changes.
     */
    public void setFilter() {
        ImageView searchButton = (ImageView) findViewById(R.id.searchButton);
        searchButton.setVisibility(View.VISIBLE);
        final EditText editTextFilter = ((EditText) findViewById(R.id.filterEditText));
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.searchButtonClicked(editTextFilter);
            }
        });
        editTextFilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                presenter.filterTextChanged(cs);
            }
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
            @Override
            public void afterTextChanged(Editable arg0) {
            }
        });
    }

    /**
     * Clears filter EditText.
     */
    public void cleanSearchEditText(){
        MainActivity.this.adapter.getFilter().filter("");
    }

    /**
     * Shows a ProgressDialog
     */
    @Override
    public void showProgress() {
        progressDialog.show();
    }

    /**
     * Hides a ProgressDialog if it is showing.
     */
    @Override
    public void hideProgress() {
        if(progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    /**
     * Informs the presenter that the list has been scrolled. This will be called after the scroll has completed.
     *
     * @param view The view whose scroll state is being reported.
     * @param firstVisible the index of the first visible cell (ignore if visibleItemCount == 0).
     * @param visibleCount the number of visible cells.
     * @param totalCount the number of items in the list adaptor.
     */
    @Override
    public void onScroll(AbsListView view,int firstVisible, int visibleCount, int totalCount) {
        presenter.onScroll(view, firstVisible, visibleCount, totalCount);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onScrollStateChanged(AbsListView v, int s) {
    }

    /**
     * Show a Dialog with gnome details
     *
     * @param item gnome to show
     * @param photo gnome thumbnail if already loaded (null if not)
     *
     */
    public void showItemDetailsDialog(final Object item, ImageView photo) {
        Gnome gnome = (Gnome)item;
        final Dialog infoDialog = new Dialog(MainActivity.this, R.style.CustomDialogStyle);
        LayoutInflater factory = LayoutInflater.from(MainActivity.this);
        final View view = factory.inflate(R.layout.gnome_details_dialog, null);
        Typeface typeFace = Typeface.createFromAsset(this.getAssets(), "fonts/AaronSans.ttf"); //$NON-NLS-1$
        //If image already downloaded, use it
        if (photo != null && photo.getDrawable() != null){
            ((ImageView) view.findViewById(R.id.photoImageViewDialog)).setImageDrawable(photo.getDrawable());
        }else{
            ImageView thumb = (ImageView) view.findViewById(R.id.photoImageViewDialog);
            String imageUri = gnome.getThumbnail();
            Picasso.with(MainActivity.this).load(imageUri).into(thumb);
        }
        TextView nameTV = (TextView) view.findViewById(R.id.nameTextViewDialog);
        formatTextView(nameTV, 23, typeFace, getResources().getColor(R.color.android_green));
        nameTV.setText(gnome.getName());
        ((TextView) view.findViewById(R.id.ageTextViewDialog)).setText(getString(R.string.Age_value, gnome.getAge()));
        ((TextView) view.findViewById(R.id.weightTextViewDialog)).setText(getString(R.string.Weight_value, gnome.getWeight()));
        ((TextView) view.findViewById(R.id.heightTextViewDialog)).setText(getString(R.string.Height_value, gnome.getHeight()));
        ((TextView) view.findViewById(R.id.hairColorTextViewDialog)).setText(getString(R.string.Hair_color_value, gnome.getHair_color()));
        ((TextView) view.findViewById(R.id.professionsTextViewDialog)).setText(getString(R.string.Professions_)+" "+gnome.getProfessionsString());
        addGnomeFriendsToDialog(gnome, infoDialog, view);
        infoDialog.setContentView(view);
        infoDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        infoDialog.show();
    }

    /**
     * Shows a dialog with an error message.
     *
     * @param errorMessage The error message.
     */
    public void showErrorDialog(String errorMessage){
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle(getString(R.string.error));
        alertDialog.setMessage(errorMessage);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.exit),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        presenter.onExit();
                    }
                });
        alertDialog.show();
    }

    /**
     * Adds a set of clickable TextViews to a dialog with the names of a gnome's friends.
     * When one of these TextViews is clicked the Presenter of the MVP pattern will informed.
     * If the gnome has no friends a non clickable TextView will be added.
     *
     * @param gnome The gnome whose friends will be added.
     * @param dialog The dialog where TextViews will be added.
     * @param view The view of the dialog.
     */
    private void addGnomeFriendsToDialog(Gnome gnome, final Dialog dialog, View view) {
        LinearLayout.LayoutParams p1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        p1.setMargins(50, 5, 0, 0);
        LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.layoutDialog);

        if(gnome.getFriends() != null && gnome.getFriends().length>0) {
            for (int i = 0; i < gnome.getFriends().length; i++) {
                final String friend = gnome.getFriends()[i];
                TextView tempFriend = createTextViewWithText(gnome.getFriends()[i], p1);
                formatTextView(tempFriend, -1, null, getResources().getColor(R.color.DodgerBlue));
                tempFriend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dialog != null && dialog.isShowing()) dialog.dismiss();
                        presenter.friendClicked(friend);
                    }
                });
                linearLayout.addView(tempFriend);
            }
        }else{
            TextView friend = createTextViewWithText(getString(R.string.nobody), p1);
            linearLayout.addView(friend);
        }
    }

    /**
     * Creates a TextView with the specified text.
     *
     * @param text The text that will show the TextView.
     * @param layoutParams The layout params for the TextView.
     * @return a TextView with specified text.
     */
    @NonNull
    private TextView createTextViewWithText(String text, LinearLayout.LayoutParams layoutParams) {
        TextView tv = new TextView(MainActivity.this);
        tv.setText(text);
        tv.setLayoutParams(layoutParams);
        return tv;
    }

    /**
     * Format TextView with specified parameters
     *
     * @param tv TextView to format
     * @param size size of the TextView
     * @param typeFace font of the TextView
     * @param idColor color id of the TextView
     *
     */
    public void formatTextView(TextView tv,  int size, Typeface typeFace, int idColor){
        if(typeFace!= null) tv.setTypeface(typeFace);
        if(size > 1 )tv.setTextSize(size);
        tv.setTextColor(idColor);
    }

    /**
     * @return the presenter object of the MVP pattern.
     */
    public Presenter getPresenter(){
        return presenter;
    }

    /**
     * @return the adapter of the ListActivity.
     */
    public CustomListAdapter getAdapter(){
        return adapter;
    }
}
